export const DASHBOARD = '/dashboard'
export const ASSETS = DASHBOARD + '/assets'

export const SIGNIN = '/signin'
export const SIGNUP = '/signup'
export const PASSWORD_FORGET = '/forgetpassword'