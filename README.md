# A Tool for Renting Company

Project -> https://expert-tool-hs-aalen.web.app/

To run this project follow this steps:

## NPM

1. `npm install`

2. ` npm run dev`

App locally on port 3000

## YARN

1. `yarn`

2. `yarn dev`
