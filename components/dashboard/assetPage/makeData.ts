import faker from "@faker-js/faker";
import { AssetTableObject } from "types/global";



const range = (len: number) => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = (): AssetTableObject => {
  return {
    date: faker.date.past().toDateString(),
    status: faker.random.word(),
    destination: faker.address.streetAddress(),
    confirmed: false
  };
};

export function makeData(...lens: number[]) {
  const makeDataLevel = (depth = 0): AssetTableObject[] => {
    const len = lens[depth]!;
    return range(len).map((d): AssetTableObject => {
      return {
        ...newPerson(),
      };
    });
  };

  console.log(makeDataLevel());

  return makeDataLevel();
}
