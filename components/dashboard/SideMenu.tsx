import {
  Flex,
  Line,
  Navigation,
  Tab,
  Tabs,
  TabsHeader,
} from "@findnlink/neuro-ui";
import { auth, signOut } from "lib/firebase";
import { logout } from "lib/slices/userSlice";
import { useRouter } from "next/router";
import toast from "react-hot-toast";
import { FaFileInvoiceDollar } from "react-icons/fa";
import { FiGrid, FiLogOut, FiSettings } from "react-icons/fi";
import { GoLocation, GoTools } from "react-icons/go";
import { useDispatch } from "react-redux";
import { ASSETS } from "../../lib/constants/routes";
import styles from "./Dashboard.module.scss";

function SideMenu() {
  const router = useRouter();
  const dispatch = useDispatch();

  const signinOut = () => {
    signOut(auth)
      .then(() => {
        console.log("Sign-out successful.");
        dispatch(logout());
      })
      .catch((error) => {
        toast.error("An error happened.");
      });
  };

  return (
    <Navigation type="side" _class={styles.sideMenu}>
      <Tabs direction="column" id="navigation" hover>
        <TabsHeader padding="xl">
          <Tab index={0} onClick={() => router.push(ASSETS)}>
            <span className={styles.tab}>
              <FiGrid />
              Assets
            </span>
          </Tab>
        </TabsHeader>
      </Tabs>
      <Flex alignItems="center">
        <div className={styles.logoutWrapper} onClick={signinOut}>
          <span className={styles.tab}>
            <FiLogOut />
            Logout
          </span>
        </div>
      </Flex>
    </Navigation>
  );
}

export default SideMenu;
